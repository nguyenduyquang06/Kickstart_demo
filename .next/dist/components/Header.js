'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _semanticUiReact = require('semantic-ui-react');

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _jsxFileName = 'E:\\Solidity\\Practice\\Kickstart\\components\\Header.js';

exports.default = function () {
    return _react2.default.createElement(_semanticUiReact.Menu, { style: { marginTop: '20px' }, __source: {
            fileName: _jsxFileName,
            lineNumber: 6
        }
    }, _react2.default.createElement(_semanticUiReact.Menu.Item, {
        __source: {
            fileName: _jsxFileName,
            lineNumber: 7
        }
    }, 'CrowdCoin'), _react2.default.createElement(_semanticUiReact.Menu.Menu, { position: 'right', __source: {
            fileName: _jsxFileName,
            lineNumber: 8
        }
    }, _react2.default.createElement(_semanticUiReact.Menu.Item, {
        __source: {
            fileName: _jsxFileName,
            lineNumber: 9
        }
    }, 'Campaigns'), _react2.default.createElement(_semanticUiReact.Menu.Item, {
        __source: {
            fileName: _jsxFileName,
            lineNumber: 10
        }
    }, '+')));
};
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbXBvbmVudHNcXEhlYWRlci5qcyJdLCJuYW1lcyI6WyJNZW51IiwiUmVhY3QiLCJtYXJnaW5Ub3AiXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLEFBQVE7O0FBQ1IsQUFBTyxBQUVQOzs7Ozs7OztrQkFBZSxZQUFNLEFBQ2pCOzJCQUNJLEFBQUMsdUNBQUssT0FBTyxFQUFDLFdBQWQsQUFBYSxBQUFZO3NCQUF6Qjt3QkFBQSxBQUNJO0FBREo7S0FBQSxrQkFDSyxjQUFELHNCQUFBLEFBQU07O3NCQUFOO3dCQUFBO0FBQUE7QUFBQSxPQURKLEFBQ0ksQUFDQSw4QkFBQyxjQUFELHNCQUFBLEFBQU0sUUFBSyxVQUFYLEFBQW9CO3NCQUFwQjt3QkFBQSxBQUNJO0FBREo7dUJBQ0ssY0FBRCxzQkFBQSxBQUFNOztzQkFBTjt3QkFBQTtBQUFBO0FBQUEsT0FESixBQUNJLEFBQ0EsOEJBQUMsY0FBRCxzQkFBQSxBQUFNOztzQkFBTjt3QkFBQTtBQUFBO0FBQUEsT0FMWixBQUNJLEFBRUksQUFFSSxBQUlmO0FBVkQiLCJmaWxlIjoiSGVhZGVyLmpzIiwic291cmNlUm9vdCI6IkU6L1NvbGlkaXR5L1ByYWN0aWNlL0tpY2tzdGFydCJ9
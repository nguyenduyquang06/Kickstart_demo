import React, {Component} from 'react';
import Layout from '../../components/Layout';
import {Form,Button,Input,Message} from 'semantic-ui-react';
import Head from 'next/head';
import factory from '../../ethereum/factory';
import web3 from '../../ethereum/web3';
class CampaignNew extends Component {
    state = {
        minimumContribution: '',
        errorMsg: '',
        loading: false
    }

    onSubmit = async (event) => {
        event.preventDefault();
        this.setState({loading:true, errorMsg:''});
        try {
            const accounts = await web3.eth.getAccounts();
            await factory.methods.createCampaign(this.state.minimumContribution).send({
            from: accounts[0]
        });
        } catch (err) {
            this.setState({errorMsg: err.message});
        }
        this.setState({loading:false});
    };

    render() {
        return (
            <Layout>
                <Head>
                    <title>Test</title>
                </Head>
              <h2>Create a campaign</h2>
              <Form onSubmit={this.onSubmit} error={!!this.state.errorMsg} >
                  <Form.Field>
                      <label>Minimum contribution</label>
                      <Input value={this.state.minimumContribution} label="wei" labelPosition="right" onChange={(event) => {this.setState({minimumContribution:event.target.value})}} />
                  </Form.Field>
                  <Message error header="Oops!" content={this.state.errorMsg} />                  
                  <Button loading={this.state.loading} primary>Create</Button>
              </Form>              
            </Layout>
        );
    }
};

export default CampaignNew;
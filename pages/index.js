import React, {Component} from 'react';
import {Card , Button} from 'semantic-ui-react';
import factory from '../ethereum/factory';
import Layout from '../components/Layout';
class CampaignIndex extends Component {
    static async getInitialProps() {
        const campaigns = await factory.methods.getDeployedCampaigns().call();
        return {campaigns};
    }
    
    async componentDidMount() {
        
        console.log(campaigns);
    }

    renderCampaigns() {
        const items = this.props.campaigns.map(address => {
            return {
                header: address,
                description: <a>View campaigns</a>,
                fluid: true
            }
        });

        return <Card.Group items= {items}/>;
    }

    render() {
            return (
                <Layout>
                    <h3>Open Campaigns</h3>                    
                    <Button
                        floated="right" 
                        content = "Create Campaign"
                        icon ="add square"
                        primary
                    ></Button>
                    <div>{this.renderCampaigns()}</div>                    
                </Layout>            
            );
    }
}

export default CampaignIndex;
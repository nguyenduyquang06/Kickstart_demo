import Web3 from 'web3';

let web3;

if (typeof window !== 'undefined' && typeof window.web3 !== 'undefined') {
    web3 = new Web3(window.web3.currentProvider);
} else {
    // we're on the server or the user isn't running metamask
    const provider = new Web3.providers.HttpProvider('https://ropsten.infura.io/OuE2BVwDYYh4Iucxac4d');
    web3 = new Web3(provider);    
}
export default web3;